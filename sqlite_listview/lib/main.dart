import 'package:flutter/material.dart';
import 'package:sqlite_listview/screen/listview_note.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final appTitle = "Welcome To CTrends Software & Services Ldt.";

    return MaterialApp(
      title: appTitle,
      debugShowCheckedModeBanner: false,
      home: ListViewNote(),
    );
  }
}

